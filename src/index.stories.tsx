/*
 * @Author: zoufengfan
 * @Date: 2022-10-14 09:49:11
 * @LastEditTime: 2022-10-31 15:39:36
 * @LastEditors: zoufengfan
 */
import { ComponentStory, ComponentMeta } from '@storybook/react';
import React, { useState } from 'react';

import AddAndDetail from './index';

type Args = Parameters<typeof AddAndDetail>[0];

export default {
  title: 'Components/AddAndDetail',
  component: AddAndDetail,
  args: {},
} as ComponentMeta<typeof AddAndDetail>;

export const Add = (args: Args) => <AddAndDetail {...args} />;
Add.args = {
  type: 'add',
  columns: [
    {
      title: 'field A',
      dataIndex: 'aaa',
    },
    {
      title: 'field b',
      dataIndex: 'bbb',
    },
  ],
  title: 'add title',
  trigger: <button>show add</button>,
  onFinish(values, closeModalFn, unCloseModalFn) {
    closeModalFn();
  },
} as Args;

const Template: ComponentStory<typeof AddAndDetail> = (args: Args) => {
  const [visible, setvisible] = useState(false);
  return (
    <>
      <button onClick={() => setvisible(true)}>open</button>
      <AddAndDetail {...args} visible={visible} visibleAction={setvisible} />
    </>
  );
};

export const Edit = Template.bind({});
Edit.args = {
  type: 'edit',
  columns: [
    {
      title: 'field A',
      dataIndex: 'aaa',
    },
    {
      title: 'field b',
      dataIndex: 'bbb',
    },
  ],
  title: 'edit title',
  request() {
    return Promise.resolve({
      aaa: 'field a',
      bbb: 'field b',
    });
  },
  onFinish(values, closeModalFn, unCloseModalFn) {
    closeModalFn();
  },
} as Args;

export const Detail = Template.bind({});
Detail.args = {
  type: 'detail',
  title: 'detail title',
  columns: [
    {
      title: 'header ddd',
      valueType: 'group',
      columns: [
        {
          title: 'field A',
          dataIndex: 'aaa',
        },
        {
          title: 'field b',
          dataIndex: 'bbb',
        },
      ],
    },
  ],
  request() {
    return Promise.resolve({
      aaa: 'field a',
      bbb: 'field b',
    });
  },
} as Args;
