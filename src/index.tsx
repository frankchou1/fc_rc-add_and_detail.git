/*
 * @Author: zoufengfan
 * @Date: 2022-08-19 17:23:28
 * @LastEditTime: 2022-11-04 14:14:48
 * @LastEditors: zoufengfan
 */
import React, { useImperativeHandle } from 'react';
import type { RefObject } from 'react';
import Add from './Add';
import Detail from './Detail';
import type { Props as AddProps } from './Add';
import type { Props as DetailProps } from './Detail';
import type { FormInstance } from 'antd/es/form/Form';

type outPutRefs = {
  form?: FormInstance;
};

type Props<Item> = (
  | (Omit<AddProps<Item>, 'request'> & {
      type: 'add';
    })
  | (AddProps<Item> & {
      type: 'edit';
    })
  | (DetailProps<Item> & {
      type: 'detail';
    })
) & {
  /**定义子组件暴露的方法 */
  cRef?: RefObject<outPutRefs>;
};

function AddAndDetail<Item>(props: Props<Item>) {
  const { cRef } = props;
  //  暴露方法到外部
  useImperativeHandle(
    cRef,
    (): outPutRefs => ({
      form: cRef?.current?.form,
    }),
  );
  return (
    <div>{props.type === 'detail' ? <Detail<Item> {...props} /> : <Add<Item> {...props} />}</div>
  );
}

export default AddAndDetail;
